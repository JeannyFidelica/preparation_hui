/** @type {import('tailwindcss').Config} */

export default {
  content: [
    './index.html',
    './src/**/*.{js,ts,jsx,tsx}',
    './node_modules/tw-elements-react/dist/js/**/*.js',
  ],
  theme: {
    extend :{
        
      colors: { 
        transparent: 'transparent',
        current: 'currentColor',
        'primary': '#a73cc8',
        'secondary': '#e298a6',
        'accent': '#d47868',
        'text': '#19071d',
        'background': '#eeeaf0',
        'dark-primary': '#a237c3',
        'dark-secondary': '#671d2b',
        'dark-accent': '#973b2b',
        'dark-text': '#f3f1f4',
        'dark-background': '#130f15',
      },
    }
  },
  plugins: [require('tw-elements-react/dist/plugin.cjs')],
  // darkMode: 'true',
}
