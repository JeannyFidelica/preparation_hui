import { Icon } from "../atoms/Icon";
import { Logo } from "../atoms/Logo";
import { Time } from "../atoms/Time";

export function Main() {
  return (
    <>
      <div className="justify-center items-center h-screen p-5 text-text dark:text-dark-text">
        <div>
           <header className="flex justify-between items-center p-4 bg-transparent text-white">
            <Time/>
            <Icon />
          </header> 
          <Logo />
        </div>
        <div className="flex flex-col justify-center bg-transparent">
          <div className="flex items-center">
            <div className="text-left">
              <div className="flex flex-col md:flex-row">
                <div className="text-left md:w-2/3">
                  <div className="bg-transparent p-4 sm:p-0 rounded-lg">
                    <div className="sm:my-4">
                      <h1 className="text-xl md:text-2xl xl:text-3xl md:my-5 font-bold mb-2 text-text dark:text-dark-text">LANDINGPAGE WEBSITE</h1>
                      <p className="text-sm md:text-md xl:text-lg">Lorem na lacinia arcu, non mattis mi nisi nec felis. Nullam aliquet est eu mi varius, eget malesuada turpis fermentum. Maecenas nec mi eros. Nunc vel sapien eu nisl feugiat feugiat. Integer pharetra, est eget sagittis hendrerit, turpis ltrices efficitur. Cras feugiat eros vel odio iaculis, ac ultricies justo lacinia.</p>
                    </div>
                  </div>
                </div>
                <div className="flex justify-center md:justify-end">
                  <img
                    src="/appareil.jpeg"
                    alt="Image"
                    className="w-40 h-40 mb-5 md:w-auto md:h-auto md:mb-0"
                  />
                </div>
              </div>
              <div className="container mx-auto w-full flex justify-center md:justify-start my-5">
              <button className=" bg-primary z-[51] hover:bg-blue-500 font-bold py-2 px-4 rounded">
                  Get Started
                </button>
                <button className="bg-[#2C121C] z-[51] hover:bg-blue-500 font-bold py-2 px-4 rounded">
                  Get Started
                </button>
              
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
