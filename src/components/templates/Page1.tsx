export function Page1() {
    return (
      <>
        {/* Header de la page 1 */}
        <div className="flex flex-col justify-between bg-background dark:bg-dark-background">
          <div className=" flex flex-col justify-center gap-5 py-5 text-center">
            <h1 className="text-3xl font-bold">Features</h1>
            <p> Lorem . Phasellus sollicitudin, eros non accumsan lobortis, quam diam varius quam, at condimentum leo est non enim.</p>
          </div>
          
          {/* Main de la page 1 */}
          <div className="bg-[#ECD9E1] dark:bg-[#231119]">
          <div className="container mx-auto py-8 ">
            <div className="grid grid-cols-1 md:grid-cols-3 gap-8">
              <div className="bg-transparent  p-4">
                <img src="Desert.jpg" alt="Desert" className="w-full h-40 object-cover rounded" />
                <div className="mt-4">
                  <h3 className="text-xl font-semibold text-center text-black">LOREM IPSUM</h3>
                  <p className="text-center text-black">Description de l'image 1</p>
                </div>
              </div>
              <div className="bg-transparent  p-4">
                <img src="montagne.jpg" alt="montagne" className="w-full h-40 object-cover rounded" />
                <div className="mt-4">
                  <h3 className="text-xl font-semibold text-center text-black">LOREM IPSUM</h3>
                  <p className="text-center text-black">Description de l'image 2</p>
                </div>
              </div>
              <div className="bg-transparent  p-4">
                <img src="Mer.jpg" alt="Mer" className="w-full h-40 object-cover rounded" />
                <div className="mt-4">
                  <h3 className="text-xl font-semibold text-center text-black">LOREM IPSUM</h3>
                  <p className="text-center text-black">Description de l'image 3</p>
                </div>
              </div>
            </div>
          </div>
          </div>

            {/* footer de la page1  */}
          <div className="bg-white">

          <div className="flex justify-center items-end ">
      <form className="w-full max-w-lg bg-gray-200 p-8 rounded-lg">
        <div className="mb-4">
          <label htmlFor="input" className="block text-gray-700 font-bold mb-2">
            Input Label
          </label>
          <input
            id="input"
            type="text"
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
            placeholder="Lorem"
          />
        </div>
        <button
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
          type="button"
        >
          Submit
        </button>
      </form>
    </div>
          </div>
        </div>
  </>  
    );
}
