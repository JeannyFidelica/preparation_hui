export function Page2(){
    return(
    <>
    <div className="flex flex-col h-screen">
    {/* Header */}
    <div className="bg-white text-black text-center py-4">
      <h1 className="text-3xl font-semibold">VIDEO</h1>
    </div>

    {/* Main */}
    <div className="flex-1 overflow-hidden">
      <video className="w-full h-full object-cover" autoPlay loop muted controls>
        <source src="/dog_-_77509 (540p).mp4" type="video/mp4" />
            Video insupportable.
      </video>
    </div>

    {/* Footer */}

    <div className="bg-gray-200 py-3">
    <div className="container mx-auto flex justify-center items-center">
      <div className="w-2/3 bg-gray-500 p-8 rounded-lg">
        <div className="flex justify-between mb-4">
          <div className="w-1/2 pr-4">
            <h1 className="text-black text-lg mb-2">Lorem ipsum</h1>
            <p className="text-black">Lorem ipsum dolor sit amet, consectetur adipiscing elit.rat volutpat ultricies. Sed nec mauris quis libero efficitur scelerisque.</p>
          </div>
          <div className="w-1/2 pl-4">
            <h1 className=" text-black text-lg mb-2">Lorem ipsum</h1>
            <p className="text-black">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce hendrerit aliquam dui, nec eleifend justo sagittis eu.</p>


          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  </>
);

}
