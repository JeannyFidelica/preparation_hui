import {Icon} from "../atoms/Icon"
import {Logo} from "../atoms/Logo"
import {Time} from "../atoms/Time"

export function Header() {
    return (
      <>
        <header className="flex justify-between ,items-center, p-4 bg-transparent text-white">
          <Time />
          <Icon />
        </header>
        <Logo/>
      </>
    );
}