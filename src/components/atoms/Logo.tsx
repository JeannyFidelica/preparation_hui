import { HiMenuAlt3 } from "react-icons/hi";

export function Logo() {
  return (
    
      <div className="flex items-center justify-between">
        <div className="flex">
          <img
            src="/logo.jpg"
            alt="Logo"
            className="w-12 h-12 ml-10 mb-5" 
          />
          <p>Logo</p>
        </div>
        <HiMenuAlt3 className="h-6 w-6 mr-10 cursor-pointer " />
      </div> 
   
  );
}