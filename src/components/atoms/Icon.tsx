import { CiBatteryFull } from "react-icons/ci";
import { CiWifiOn } from "react-icons/ci";
import { GiNetworkBars } from "react-icons/gi";

export function Icon() {
    return (
      <div className="flex items-center space-x-4">
        <CiBatteryFull className="h-6 w-6 text-red-500"/>
        <GiNetworkBars className="h-4 w-4"/> 
        <CiWifiOn className="h-6 w-6"/>
      </div>
    );
  }
  