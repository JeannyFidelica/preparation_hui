import clsx from "clsx";
import { PropsWithChildren, useEffect, } from "react";
import { useDispatch, useSelector } from "react-redux";
import { UserLoading, selectAllUsers } from "../../store/modules/user/User.selector";
import { userLoadRequested } from "../../store/modules/user/User.action";

type ButtonProps = PropsWithChildren<{
	className?: string | undefined
	text?: string |undefined,
	noBordure?:boolean
	// onClick?:()=>void
}>;

function Button({ children, className, text, noBordure}: ButtonProps) {
	const dispatch = useDispatch();
    const loading = useSelector(UserLoading);
    const data = useSelector(selectAllUsers);
    useEffect(() => {
        dispatch(userLoadRequested());
    }, [])
    console.log("===>", data)
	
	return (
		<button className={clsx(
			className,
			'flex justify-between gap-0 items-center',
			noBordure?"border-0":' border-2 border-gray-400',
			'py-3 px-6',
			'rounded-xl',
			'text-lg',
			'w-fit',
			)}>
			<>
				{children}
				{text}
                {/* {
                    loading ? "LOADING" : "TEXT"
                }  */}
			</>
		</button>);
}
export function Test() {
	return (
		<button>
			<div>+</div>
		</button>);
}


export default Button;