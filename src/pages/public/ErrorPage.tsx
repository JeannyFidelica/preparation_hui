import {Link } from "react-router-dom";

function ErrorPage() {
  // const error:any = useRouteError();

  return (
    <div id="error-page">
      <div className="min-h-screen flex flex-grow items-center justify-center background-color-login">
      <div className="rounded-lg bg-white p-8 text-center shadow-xl">
        <h1 className="mb-4 text-4xl font-bold">404</h1>
        <p className="text-gray-600">
        {"         "} Page introuvable {"         "}
        </p>
        <Link to="/"
          className="mt-4 inline-block rounded px-4 py-2 font-semibold text-black"
        >
          {" "}
          Retour à l'accueil{" "}
        </Link>
      </div>
    </div>
    </div>
  );
}
export default ErrorPage;