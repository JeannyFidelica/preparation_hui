import {Title} from "rizzui"
import Button from "../../components/atoms/Button"
import {Header} from "../../components/layout/index"
import {Main} from "../../components/templates/index"
import { Page1 } from "../../components/templates/Page1"
import { Page2 } from "../../components/templates/Page2"

export function HomePage(){
return(
<>
  <div className="bg-cover bg-center bg-no-repeat " style={{backgroundImage: "url('/background.jpg')"}}>
    <div>
      <Main />
      <Page1 />
      <Page2 />
    </div>
  </div>

</>

)}

const App = () => {
  return (
    <div>
      <HomePage />
    </div>
  );
}

export default App;
