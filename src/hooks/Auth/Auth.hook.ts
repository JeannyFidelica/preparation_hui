import { createContext, useContext, useMemo } from "react";
import { useNavigate } from "react-router-dom";
import { UseLocalStorage } from "../storage/UseLocalStorage.hook";
import { useDispatch } from "react-redux";
import { IUser } from "@/@schema";
import { userLogin } from "@/store/redux/User";

interface AuthContextProps {
  user: IUser | null;
  login: (data:IUser) => Promise<void>;
  logout: () => void
}
const AuthContext = createContext<AuthContextProps>({
  user: null,
  login: async () => {
    return Promise.resolve()
  },
  logout: () => { }
});

export const AuthProvider = ({ children }: any) => {
  const [user, setUser] = UseLocalStorage("user", null);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const login = async (data: any) => {
    dispatch(userLogin(data))
    navigate("/");
  };

  const logout = () => {
    setUser(null);
    navigate("/", { replace: true });
  };

  const value = useMemo<AuthContextProps>(
    () => ({
      user,
      login,
      logout,
    }),
    [user]
  );
  return <AuthContext.Provider value={ value }> { children } < /AuthContext.Provider>;
};

export const useAuth = () => {
  return useContext(AuthContext);
};