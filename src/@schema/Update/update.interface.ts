export interface IPayload<T> {
    id: string;
    payload: T
}