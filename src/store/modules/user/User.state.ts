import { EntityState } from "@reduxjs/toolkit";
import { IUser } from "../../../@schema";

export interface UserState extends EntityState<IUser, any> {
    userToken: any;
    loading: boolean;
    errorMessage: string;
    saving: boolean;
    user: IUser | undefined;
}

export interface AccountState extends EntityState<IUser, any> {
    loading: boolean,
    userInfo: IUser | null,
    userToken: string,
    errorMessage: string,
}