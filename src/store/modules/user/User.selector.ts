import { RootState } from "../configuration";
import { UserAdapter } from "./User.reducers";

export const {
    selectAll: selectAllUsers,
    selectById: selectUserById,
    selectIds: selectUserIds,
    selectTotal: selectTotalUsers
} = UserAdapter.getSelectors((state: RootState) => {
    return state.user
});

export const UserLoading = (state: RootState) => state.user.loading;
export const UserError = (state: RootState) => state.user.errorMessage;