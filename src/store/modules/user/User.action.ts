const USER = "USER"

import { createAsyncThunk } from "@reduxjs/toolkit";
import { IUser } from "../../../@schema";
import { CreateUserSerivce, LoginUserService, RegisterUserService, getUserService } from "../../../services/user/User.service";

export const userLoadRequested = createAsyncThunk(
    `${USER}/load request`,
    async (_, thunkAPI) => {
        try {
            const response = await getUserService(thunkAPI);
            return response.data;
        } catch (error) {
            return thunkAPI.rejectWithValue(error);
        }
    }
)


export const userSaveRequested = createAsyncThunk(
    `${USER}/save request`,
    async (data: IUser, thunkAPI) => {
        try {
            const response = await CreateUserSerivce(data, thunkAPI);
            return response.data;
        } catch (error) {
            return thunkAPI.rejectWithValue(error);
        }
    }
);

export const userRegister = createAsyncThunk(
    `${USER}/register request`,
    async (data: IUser, thunkAPI) => {
        try {
            const response = await RegisterUserService(data, thunkAPI);
            return response.data;
        } catch (error) {
            return thunkAPI.rejectWithValue(error);
        }
    }
)

export const userLogin = createAsyncThunk(
    `${USER}/login request`,
    async (data: IUser, thunkAPI) => {
        try {
            const response = await LoginUserService(data, thunkAPI);
            return response.data;
        } catch (error) {
            return thunkAPI.rejectWithValue(error);
        }
    }
)

