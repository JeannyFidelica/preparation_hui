import { EntityAdapter, PayloadAction, createEntityAdapter, createReducer } from "@reduxjs/toolkit";
import { UserState } from "./User.state";
import { userLoadRequested, userRegister } from "./User.action";
import { IUser } from "../../../@schema";

export const UserAdapter: EntityAdapter<IUser, any> = createEntityAdapter(
    {
        selectId: (entry) => entry.id
    }
)

const initialState: UserState = UserAdapter.getInitialState(
    {
        loading: false,
        saving: false,
        errorMessage: "",
        user: undefined,
        userToken: "",
    }
)


export const UserReducer = createReducer(initialState, (builder) => {
    builder
        .addMatcher(
            (action:PayloadAction) => action.type.endsWith('/pending'),
            (state) => {
                {
                    state.loading = true;
                    state.errorMessage = "";
                }
            }
        )
        .addMatcher(
            (action:PayloadAction) => action.type.endsWith('/rejected'),
            (state, action:PayloadAction) => {
                state.loading = false;
                state.errorMessage = action?.error?.message || "an error occurred";
            }
        )
        .addMatcher(
            (action: PayloadAction) => action.type.endsWith('/fulfilled'),
            (state, action: PayloadAction) => {
                if (action.type === userLoadRequested.fulfilled.type) {
                    UserAdapter.setAll(state, action.payload);
                } else if (action.type === userRegister.fulfilled.type) {
                    state.userToken = action.payload.token;
                }
                state.loading = false;
                state.errorMessage = "";
            }
        )
});
