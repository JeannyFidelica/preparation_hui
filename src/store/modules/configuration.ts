import { combineReducers } from "@reduxjs/toolkit";
import { UserReducer } from "./user/User.reducers";
import infoSlice from "../slice/info";

const rootReducer = combineReducers(
    {
        user : UserReducer,
        info: infoSlice.reducer,
    }
)
export default rootReducer;
export type RootState = ReturnType<typeof rootReducer>;