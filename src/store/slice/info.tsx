import { createSlice } from "@reduxjs/toolkit";

const infoSlice = createSlice({
    name:"info",
    initialState:{
        label:""
    },
    reducers:{
        updateInfo : (state,action)=>{    
            state.label=action.payload.label;
            
        }
    }
})
//action
export const {updateInfo}=infoSlice.actions;

export default infoSlice;
