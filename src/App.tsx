import { Suspense } from 'react'
import './index.css'
import 'tailwindcss/tailwind.css'
import { createBrowserRouter, Outlet, RouterProvider } from 'react-router-dom'
import './index.css'
import { Provider } from 'react-redux'
import { ErrorPage, HomePage } from './pages/public'
import store from './store'

const router = createBrowserRouter([
  // Public
  {
    path: '',
    errorElement: <ErrorPage />,
    element: <Outlet/>,
    children: [
      {
        path: '',
        element: <HomePage/>,
      }
    ],
  },
  // Private
  {
    path: '',
    errorElement: <ErrorPage />,
    element: <Outlet />,
    children: [
      {
        path: 'home',
        element: (
          <Suspense fallback={<div>Loading...</div>}>
            <div>Private home</div>
          </Suspense>
        ),
      },
    ],
  }
])

function App() {
  return (
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  )
}

export default App
