import { IUser } from "../../@schema";
import { IPayload } from "../../@schema/Update/update.interface";
import MainService from "../Main.service";
const API = "users"
export const getUserService = async (thunkAPI: { rejectWithValue: (arg0: unknown) => any; }) => {
    try {
        const response = await MainService._GET<IUser>(API)
        return response;
    } catch (error) {

        return thunkAPI.rejectWithValue(error);
    }
}

export const updateUserSerivce = async (data: IPayload<IUser>, thunkAPI: { rejectWithValue: (arg0: unknown) => any; }) => {
    try {
        const response = await MainService._UPDATE<IUser>(API, data);
        return response;
    } catch (error) {
        return thunkAPI.rejectWithValue(error);
    }
}

export const deleteUserSerivce = async (data: IPayload<IUser>, thunkAPI: { rejectWithValue: (arg0: unknown) => any; }) => {
    try {
        const response = await MainService._DELETE<IUser>(API, data.id);
        return response;
    } catch (error) {
        return thunkAPI.rejectWithValue(error);
    }
}
export const CreateUserSerivce = async (data: IUser, thunkAPI: { rejectWithValue: (arg0: unknown) => any; }) => {
    try {
        const response = await MainService._POST<IUser>(API, data);
        return response;
    } catch (error) {
        return thunkAPI.rejectWithValue(error);
    }
}

export const RegisterUserService = async (data: IUser, thunkAPI: { rejectWithValue: (arg0: unknown) => any; }) => {
    try {
        const response = await MainService._POST<IUser>("auth/register", data);
        return response;
    } catch (error) {
        return thunkAPI.rejectWithValue(error);
    }
}

export const LoginUserService = async (data: IUser, thunkAPI: { rejectWithValue: (arg0: unknown) => any; }) => {
    try {
        const response = await MainService._POST<IUser>("auth/login", data);
        return response;
    } catch (error) {
        return thunkAPI.rejectWithValue(error);
    }
}