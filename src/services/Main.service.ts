import { AxiosResponse } from "axios";
import { API } from "../utils/api";
import { IPayload } from "../@schema/Update/update.interface";

class MainService {

    constructor() { null;}

    public _GET<T>(api: string): Promise<AxiosResponse<T>> {
        return API.get(api);
    }
    public _POST<T>(api: string, payload: T): Promise<AxiosResponse<T>> {
        return API.post(api, payload);
    }
    public _DELETE<T>(api: string, id: string): Promise<AxiosResponse<T>> {
        return API.delete(`${api}/${id}`);
    }
    public _UPDATE<T>(api: string, { id, payload }: IPayload<T>): Promise<AxiosResponse<T>> {
        return API.put(`${api}/${id}`, payload);
    }
}
export default new MainService()